# BIOS824 Sequence Mapping

This is a course project for Seqencing Mapping.

This repository contains functions to read in fasta or fastq file.

We use Probablistic Error Model to calculate the matching accurancy.

For detailed Information of each function and the whole package. Please refer to the vignette page and documentations along with each function.

To install this repositroy, please use the following command in R:

`devtools::install_gitlab("zs68/bios824-sequence-mapping",host = "https://gitlab.oit.duke.edu",build = TRUE,build_opts = c("--no-resave-data"))`

This package contains two small sample files: `my.gen.fasta` and `my.gen.fastq`, with 500 sequences and 10000 sequences, respectively.

The general work flow to use this package is pretty simple.
```{r}
library(casestudy2)

fasta = system.file("extdata","my.gen.fasta",package = "casestudy2")
fastq = system.file("extdata","my.gen.fastq",package = "casestudy2")

reads = ReadFastgene(fastq)
reference = ReadFastgene(fasta,type = "fasta")

tbl = CreateTable_ham(reads,reference)
```

This chunck of code will generate two tables, one is for the proportion, the other is for comprehensive summary of mapping. 

