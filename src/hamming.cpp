#include <Rcpp.h>
#include <string>
using namespace Rcpp;

//' Computed Hamming Distance
//'
//' \code{hamming} computes the hamming distance between two equal length strings. The hamming distance is the number of different characters between the strings.
//' @param str1 is the first input string
//' @param str2 is the second input string
//' @return number of different character
//' @export
// [[Rcpp::export]]
int hamming(const std::string& str1, const std::string& str2) {
  int sdist = 0;

  for(int i = 0; i < str1.length(); ++i) {
    if(str1[i] != str2[i]) {
      sdist++;
    }
  }
  return(sdist);
}
